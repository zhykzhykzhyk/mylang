program queen()
type arr is array of 101 integer;
function pd(ref a, i, j)
  var a is arr;
  var i is integer;
  var j is integer;
  return boolean;
is
  var k is integer;
begin
  for k:= 1 to i-1 do
    if i + j == a[k] + k or i - j == k - a[k] or a[k] ==j then
      return no;
    end if
  end for
  return yes;
end function pd;

function queen1(i, n, ref t, ref a)
  var i is integer;
  var n is integer;
  var t is integer;
  var a is arr;
is
  var j is integer;
begin
  if i > n then
    t := t + 1;
  else
    for j:=1 to n do
      if pd(ref a, i, j) then
        a[i]:=j;
        queen1(i + 1, n, ref t, ref a);
      end if
    end for
  end if
end function queen1;
is
  var n is integer;
  var t is integer;
  var i is integer;
  var a is arr;
begin
  n := input();
  t := 0;
  for i:=0 to 100 do
    a[i] := 0;
  end for
  queen1(1, n, ref t, ref a);
  println t;
end
