#!/bin/sh

FLAGS="$FLAGS"

jq -r ".code" "$1" | ./mlcc -j `jq -r ".option" "$1"` > "$2"
