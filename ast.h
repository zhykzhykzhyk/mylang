#ifndef AST_H_
#define AST_H_

#include <llvm/IR/Verifier.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <vector>
#include <string>
#include <deque>
#include <memory>
#include <stack>
#include <typeinfo>
#include <map>
#include <stdexcept>
#include <utility>
#include <assert.h>
#include <algorithm>
#include <typeindex>
#include "mylang.tab.hh"

#define DEFINE_TAG(tname) \
    struct tname { \
      constexpr tname(size_t INDEX) : INDEX(INDEX) { } \
      static constexpr const char *NAME = #tname; \
      size_t INDEX; \
    }

/*
static llvm::Module *theModule;
static llvm::LLVMContext &context = llvm::getGlobalContext();
static llvm::IRBuilder<> builder(context);
static std::map<std::string, Value*> namedValues;
*/

class CodeGenerator {
 public:
  auto& builder() {
    return builder_;
  }
  auto& context() {
    return llvm::getGlobalContext();
  }
  auto module() {
    return &module_;
  }

  CodeGenerator(std::string name) :
    builder_(context()), module_(name, context()) { }
 private:
  llvm::IRBuilder<> builder_;
  llvm::Module module_;
};

template <size_t N, size_t i, typename... T>
struct Select;


template <size_t N, size_t i>
struct Select<N, i> {
  using type = std::tuple<>;
};

template <size_t N, typename T, typename... Remain>
struct Select<N, 0, T, Remain...> {
  using type = decltype(
      tuple_cat(std::declval<std::tuple<T>>(),
                std::declval<typename Select<N, N-1, Remain...>::type>()));
};

template <size_t N, size_t i, typename T, typename... Remain>
struct Select<N, i, T, Remain...> {
  using type = typename Select<N, i-1, Remain...>::type;
};

template <typename A, typename B>
struct Zip {
  template <typename... T1, typename... T2>
  static std::tuple<std::pair<T1, T2>...> helper(
      std::tuple<T1...>&&,
      std::tuple<T2...>&&);

  using type = decltype(helper(std::declval<A>(), std::declval<B>()));
};
#if 0
template <typename T, typename... List>
struct Find {
  template <typename... T1, typename... T2>
  static std::tuple<std::pair<T1, T2>...> helper(
      std::tuple<T1...>&&,
      std::tuple<T2...>&&);

  using type = decltype(helper(std::declval<A>(), std::declval<B>()));
};
#endif

DEFINE_TAG(length);
DEFINE_TAG(type);
DEFINE_TAG(extends);
DEFINE_TAG(id);
DEFINE_TAG(var);
DEFINE_TAG(vars);
DEFINE_TAG(expr);
DEFINE_TAG(decls);
DEFINE_TAG(cond);
DEFINE_TAG(block);
DEFINE_TAG(statements);
DEFINE_TAG(yes);
DEFINE_TAG(no);
DEFINE_TAG(index);
DEFINE_TAG(str);
DEFINE_TAG(function);
DEFINE_TAG(args);
DEFINE_TAG(returns);
DEFINE_TAG(from);
DEFINE_TAG(to);
DEFINE_TAG(delta);

template <typename T, size_t... I>
constexpr T construct(std::index_sequence<I...> seq)
{
  return T{I...};
}

template <typename T>
using Ptr = std::shared_ptr<T>;

template <typename T>
class CastPointer
{
 public:
  template <typename P>
  CastPointer(P p) : ptr(std::dynamic_pointer_cast<T>(p)) {
    assert(ptr || !p);
  }
  CastPointer(std::nullptr_t) : ptr(nullptr) { }
  operator Ptr<T>() { return ptr; }
  explicit operator bool() { return (bool)ptr; }
  Ptr<T> operator->() { return ptr; }
  friend Ptr<T>&& move(CastPointer& p) { return move(p.ptr); } private:
  Ptr<T> ptr;
};

enum NodeType {
  ListFront,
  ListBack,
};

enum BuiltinTypeId {
  kBoolean,
  kInteger,
};

enum class NodeMode {
  Node,
  List,
  Integer,
  String,
};

template <typename T>
using Property = std::pair<std::string, T>;

using NodePtr = Ptr<class ASTNode>;

class ASTWalker {
 public:
  virtual void node(std::string name, NodePtr node) = 0;
  virtual void attr(std::string name, std::string value) = 0;
  virtual void attr(std::string name, int value) = 0;
  virtual ~ASTWalker() { }
};

class Finder : public ASTWalker {
  NodePtr result_;
  std::string name_;
  bool found_;
 public:
  Finder(std::string name) : name_(std::move(name)) { }

  virtual void node(std::string name, NodePtr node) {
    if (name == name_)
      result_ = node,
      found_ = true;
  }
  virtual void attr(std::string name, std::string value) override {}
  virtual void attr(std::string name, int value) override {}

  bool found() const { return found_; }
  NodePtr result() const { return result_; }
};

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif
extern YYLTYPE yylloc;
extern YYLTYPE yyloc;

class ASTNode {
 public:
  virtual ~ASTNode();
  virtual void walk(ASTWalker &w) = 0;
  
  using Value = llvm::Value;

  ASTNode() { location = yylloc; }
  YYLTYPE location;

  virtual std::string name() { return ""; }
  virtual NodeMode mode() { return NodeMode::Node; }

  virtual NodePtr get(std::string s) {
    Finder f(s);
    walk(f);
    if (!f.found())
      throw std::out_of_range("'" + s + "' is not exist.");
    return f.result();
  }

  virtual void add(NodeType type, NodePtr node) {
    assert(0);
  }

  virtual Value* codegen(CodeGenerator&) {
    assert(0);
    abort();
  }
};

class SementicError : public std::runtime_error
{
 public:
  SementicError(NodePtr where, std::string what) :
    SementicError(where->location, std::move(what)) { }

  SementicError(const YYLTYPE& where, std::string what) :
    runtime_error(std::move(what)), where_(where) {
      std::clog << "EXCEPTION" << std::endl;
    }

  const YYLTYPE& where() const { return where_; }
 private:
  YYLTYPE where_;
};

class TypeError : public SementicError
{
  using SementicError::SementicError;
};

inline ASTNode::~ASTNode() {}
template <typename T>
struct AsCastPointer;

template <typename T>
struct AsCastPointer<std::shared_ptr<T>> {
    using type = CastPointer<T>;
};

template <typename Base, typename... Properties>
class Node : public Base {
  static_assert(sizeof...(Properties) % 2 == 0, "(type, tag) pair required.");
 public:
  using Base::get;

  template <size_t I>
  decltype(auto) get() { return std::get<I>(values_); }

  template <typename Tag>
  decltype(auto) get() { return get<std::get<Tag>(keys_).INDEX>(); }

  template <typename... T, size_t... I>
  Node(std::index_sequence<I...>, T&&... val) :
       values_(typename AsCastPointer<typename std::tuple_element<I, decltype(values_)>::type>::type(
               std::forward<T>(val))...) { }

  template <typename... T>
  Node(T&&... val) : Node(std::make_index_sequence<sizeof...(T)>(),
                          std::forward<T>(val)...) {}

  void walk(ASTWalker &w) override {
      walk<0>(w);
  }

 private:
  template <size_t I>
  typename std::enable_if<I < sizeof...(Properties)/2>::type walk(ASTWalker &w)
  {
    constexpr decltype(auto) name =
        std::tuple_element<I, decltype(keys_)>::type::NAME;
    w.node(name, std::get<I>(values_));
    walk<I + 1>(w);
  }

  template <size_t I>
  typename std::enable_if<I >= sizeof...(Properties)/2>::type walk(ASTWalker &w)
  {
  }

  static constexpr auto keys_ =
      construct<typename Select<2, 1, Properties...>::type>(
          std::make_index_sequence<sizeof...(Properties) / 2>());

  typename Select<2, 0, Ptr<Properties>...>::type values_;
};

template <typename T>
class List : public std::conditional<std::is_same<T, class Statement>::value,
    class Statement, ASTNode>::type {
 public:
  List() = default;
  List(CastPointer<T> first) {
    list_.push_back(first);
  }
  void add(NodeType type, NodePtr node) override {
    CastPointer<T> ptr = node;
    switch (type) {
      case ListFront:
        return list_.push_front(move(ptr));
      case ListBack:
        return list_.push_back(move(ptr));
      default:
        assert(0);
    }
  }
  void walk(ASTWalker &w) override
  {
    for (auto&& node : list_)
      w.node("", node);
  }

  // for List<Statement> only
  void check() {
    for (auto&& node : list_)
      node->check();
  }

  llvm::Value *codegen(CodeGenerator& generator) override {
    for (auto&& e : list_)
      e->codegen(generator);
    return nullptr;
  }

  std::vector<llvm::Value *> vcodegen(CodeGenerator& generator) {
    std::vector<llvm::Value *> values;
    values.reserve(list_.size());
    transform(list_.begin(), list_.end(), back_inserter(values),
        [&generator](auto x) { return x->codegen(generator); });
    return values;
  }

  NodeMode mode() override { return NodeMode::List; }
  auto begin() { return list_.begin(); }
  auto end() { return list_.end(); }
  auto rbegin() { return list_.rbegin(); }
  auto rend() { return list_.rend(); }
  auto size() const { return list_.size(); }

 private:
  std::deque<Ptr<T>> list_;
};

using LookupTable = std::map<std::string, Ptr<class Declaration>>;

class Scope {
 public:
  class ScopeIterator : public std::iterator<Scope, std::input_iterator_tag> {
   public:
    ScopeIterator() : ScopeIterator(nullptr) { }
    Scope& operator *() { return *current; }
    Scope* operator ->() { return current; }
    ScopeIterator& operator++() { current = current->upper.get(); return *this; }
    ScopeIterator operator++(int) { ScopeIterator i = *this; ++*this; return i; }
    bool operator == (const ScopeIterator& c) const {
      return current == c.current;
    }
    bool operator != (const ScopeIterator& c) const {
      return current != c.current;
    }
    friend ScopeIterator begin(Scope&);
    friend ScopeIterator end(Scope&);
   private:
    ScopeIterator(Scope *s) : current(s) { }
    Scope *current;
  };

  friend ScopeIterator begin(Scope&);
  friend ScopeIterator end(Scope&);

  Scope(Ptr<Scope> upper = nullptr, Ptr<Scope> scope = nullptr)
      : upper(upper), scope_(scope) { }

  void add(std::string, Ptr<Declaration> d);
  void add(Ptr<Declaration> d);

  class ScopeHolder {
   public:
    ScopeHolder(ScopeHolder&&) = default;
    ~ScopeHolder() {
      assert(current_ == scope_);
      current_ = current_->upper;
      std::clog << "END SCOPE" << std::endl;
    }

    auto operator->() {
      return scope_;
    }
   private:
    ScopeHolder(const ScopeHolder&) = delete;
    ScopeHolder(Ptr<Scope> scope) : scope_(scope) { }

    Ptr<Scope> scope_;
    friend Scope;
  };

  template <typename T=Scope, typename... Targs>
  static ScopeHolder new_scope(Targs... args) {
    std::clog << "NEW SCOPE" << std::endl;
    return current_ = std::make_shared<T>(current_, args...);
  }

  Ptr<Declaration> lookup(const std::string& s) {
    for (auto&& scope : *this) {
      if (scope.scope_)
        if (auto d = scope.scope_->lookup(s))
          return d;

      auto p = scope.table_.find(s);
      if (p != scope.table_.end())
        return p->second;
    }
    return nullptr;
    //throw SementicError("Undecalared symbol name '" + s + "'.");
  }

  virtual ~Scope() { }

  static Ptr<Scope> current() { return current_; }
 private:
  Ptr<Scope> upper;
  Ptr<Scope> scope_;
  LookupTable table_;
  static Ptr<Scope> current_;
};

inline Scope::ScopeIterator begin(Scope& s) {
  return {&s};
}

inline Scope::ScopeIterator end(Scope&) {
  return {nullptr};
}

class ClassScope : public Scope {
  using Scope::Scope;
};

class FunctionScope : public Scope {
 public:
  FunctionScope(Ptr<Scope> outter, Ptr<class Type> ret, class Function *function)
      : Scope(outter), returns_(ret), function_(function) {
  }

  Ptr<Type> returns() {
    return returns_;
  }

  Function *function() {
    return function_;
  }

  static auto new_scope(Ptr<Type> ret, Function *f) {
    return Scope::new_scope<FunctionScope>(ret, f);
  }

  static Ptr<FunctionScope> current() {
    return std::dynamic_pointer_cast<FunctionScope>(Scope::current());
  }
 private:
  Ptr<Type> returns_;
  Function *function_;
};

class Statement : public ASTNode {
 public:
  // throw TypeError on error
  // no-op on success
  virtual void check() = 0;
};

class Type : public ASTNode {
 public:
  virtual bool equals(Ptr<Type> t) = 0;
  virtual void check() = 0;
  llvm::Type *value() {
    assert(value_);
    return value_;
  }

  virtual bool complete() { return true; }

 protected:
  void value(llvm::Type *value) {
    assert(!value_);
    value_ = value;
  }

  bool hasValue() {
    return value_;
  }

 private:
  llvm::Type *value_{};
};

class Expression : public ASTNode {
 public:
  virtual Ptr<Type> type() = 0;
};

class Variable : public Expression {
 public:
  virtual Value *object() { abort(); }
};

class Declaration : public ASTNode {
 public:
  virtual void check() = 0;
  virtual Value *value() { abort(); }

  std::weak_ptr<class Class> parent;
};

inline bool equals(Ptr<Type> lhs, Ptr<Type> rhs);

class Identifier : public ASTNode {
 public:
  Identifier(std::string name) : name_(name) { }
  void walk(ASTWalker &w) override { w.attr("id", name_); }
  std::string name() override { return "identifier"; }
  std::string text() const { return name_; }
 private:
  std::string name_;
};

class RefIdentifer : public Identifier {
 public:
  RefIdentifer(CastPointer<Identifier> id) : Identifier(id->text()) { }
};

inline bool equals(Ptr<Identifier> lhs, Ptr<Identifier> rhs)
{
  return lhs->text() == rhs->text();
}

class Keyword : public Identifier {
  using Identifier::Identifier;
  std::string name() override { return "keyword"; }
};

class Operator : public Identifier {
  using Identifier::Identifier;
  std::string name() override { return "operator"; }
};

class BuiltinType : public Type {
 public:
  BuiltinType(BuiltinTypeId id) : id_(id) { }
  virtual std::string name() override {
    return "typeref";
  }
  void walk(ASTWalker &w) override
  {
    w.attr("id", text());
  }
  std::string text() {
    switch (id_) {
      case kInteger:
        return "integer";
        break;
      case kBoolean:
        return "boolean";
        break;
      default:
        assert(0);
    }
  }

  int bits() {
    switch (id_) {
      case kInteger:
        return 32;
      case kBoolean:
        return 1;
      default:
        assert(0);
    }
  }

  virtual bool equals(Ptr<Type> t) override {
    auto a = std::dynamic_pointer_cast<BuiltinType>(t);
    return id_ == a->id_;
  }

  void check() override {
    // noop
  }

  Value *codegen(CodeGenerator& generator) {
    if (!hasValue())
      value(llvm::Type::getIntNTy(generator.context(), bits()));
    return nullptr;
  }
 private:
  BuiltinTypeId id_;
};

enum class Operation {
  Addition,
  Subtruction,
  Multiplication,
  Division,
  Modulo,

  LessThan,
  LessThanOrEqual,
  GreaterThan,
  GreaterThanOrEqual,

  Equal,
  NotEqual,

  And,
  Or,
};

class BinaryExpression : public Expression {
 public:
  BinaryExpression(CastPointer<Expression> left,
                   Operation op,
                   CastPointer<Expression> right) :
      left_(left), right_(right), op_(op) { }

  static const char *opname(Operation op)
  {
    switch (op) {
      case Operation::Addition:
        return "add";
      case Operation::Subtruction:
        return "sub";
      case Operation::Multiplication:
        return "mul";
      case Operation::Division:
        return "div";
      case Operation::Modulo:
        return "mod";
      case Operation::LessThanOrEqual:
        return "le";
      case Operation::LessThan:
        return "lt";
      case Operation::GreaterThan:
        return "gt";
      case Operation::GreaterThanOrEqual:
        return "ge";
      case Operation::Equal:
        return "eq";
      case Operation::NotEqual:
        return "ne";
      case Operation::And:
        return "and";
      case Operation::Or:
        return "or";
      default:
        assert(0);
    }
  }

  virtual std::string name() override {
    return opname(op_);
  }

  Ptr<Type> type() override {
    static auto itype = std::make_shared<BuiltinType>(kInteger);
    static auto btype = std::make_shared<BuiltinType>(kBoolean);
    auto ltype = left_->type();
    auto rtype = right_->type();
    switch (op_) {
      case Operation::Addition:
      case Operation::Subtruction:
      case Operation::Multiplication:
      case Operation::Division:
      case Operation::Modulo:
        if (!equals(ltype, itype) ||
            !equals(rtype, itype))
          throw TypeError(location, "both operands of the operator should be an integer.");
        return itype;
      case Operation::LessThanOrEqual:
      case Operation::LessThan:
      case Operation::GreaterThan:
      case Operation::GreaterThanOrEqual:
        if (!equals(ltype, itype) ||
            !equals(rtype, itype))
          throw TypeError(location, "both operands of the operator should be an integer.");
        return btype;
      case Operation::Equal:
      case Operation::NotEqual:
        if (!equals(ltype, rtype))
          throw TypeError(location, "both operands of the operator should have the same type.");
        if (!equals(ltype, itype) && !equals(ltype, btype))
          throw TypeError(location, "both operands of the operator should be either an integer or a boolean value.");
        return btype;
      case Operation::And:
      case Operation::Or:
        if (!equals(ltype, btype) ||
            !equals(rtype, btype))
          throw TypeError(location, "both operands of the operator should be a boolean value.");
        return btype;
      default:
        assert(0);
    }
  }

  void walk(ASTWalker &w) override
  {
    w.node("left", left_);
    w.node("right", right_);
  }

  Value* codegen(CodeGenerator& generator) override {
    auto& builder = generator.builder();
    Value* L = left_->codegen(generator);
    Value* R = right_->codegen(generator);
    if (L == 0 || R == 0) return 0;
    switch (op_) {
      case Operation::Addition:
        return builder.CreateAdd(L, R);
      case Operation::Subtruction:
        return builder.CreateSub(L, R);
      case Operation::Multiplication:
        return builder.CreateMul(L, R);
      case Operation::Division:
        return builder.CreateSDiv(L, R);
      case Operation::Modulo:
        return builder.CreateSRem(L, R);
      case Operation::LessThanOrEqual:
        return builder.CreateICmpSLE(L, R);
      case Operation::LessThan:
        return builder.CreateICmpSLT(L, R);
      case Operation::GreaterThan:
        return builder.CreateICmpSGT(L, R);
      case Operation::GreaterThanOrEqual:
        return builder.CreateICmpSGE(L, R);
      case Operation::Equal:
        return builder.CreateICmpEQ(L, R);
      case Operation::NotEqual:
        return builder.CreateICmpNE(L, R);
      case Operation::And:
        return builder.CreateAnd(L, R);
      case Operation::Or:
        return builder.CreateOr(L, R);
      default:
        assert(0);
    }
  }
 protected:
  Ptr<Expression> left_, right_;
  Operation op_;
};

class LogicalBinaryExpression : public BinaryExpression {
  using BinaryExpression::BinaryExpression;
  Value* codegen(CodeGenerator& generator) override {
    auto tb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto fb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    llvm::BasicBlock *rb, *nextb;
    switch (op_) {
      case Operation::And:
        rb = tb;
        nextb = fb;
        break;
      case Operation::Or:
        rb = fb;
        nextb = tb;
        break;
      default:
        assert(0);
    }
    Value* L = left_->codegen(generator);
    generator.builder().CreateCondBr(L, tb, fb);
    auto lb = generator.builder().GetInsertBlock();

    generator.builder().SetInsertPoint(rb);
    Value* R = right_->codegen(generator);
    generator.builder().CreateBr(nextb);
    rb = generator.builder().GetInsertBlock();

    generator.builder().SetInsertPoint(nextb);
    auto V = generator.builder().CreatePHI(L->getType(), 2);
    V->addIncoming(L, lb);
    V->addIncoming(R, rb);

    return V;
  }
};

class Integer : public Expression {
 public:
  Integer(int value) : value_(value) { }
  int val() const { return value_; }
  std::string sval() const { return std::to_string(value_); }
  virtual std::string name() override {
    return "integer";
  }

  //NodeMode mode() override { return NodeMode::Integer; }
  Ptr<Type> type() override {
    static auto itype = std::make_shared<BuiltinType>(kInteger);
    return itype;
  }

  void walk(ASTWalker &w) override
  {
    w.attr("value", value_);
  }

  Value* codegen(CodeGenerator& generator) override {
    return llvm::ConstantInt::get(generator.context(), llvm::APInt(32, value_));
  }
 private:
  int value_;
};

class String : public ASTNode {
 public:
  String(std::string str) : str_(str) { }
  Value *codegen(CodeGenerator& generator) override {
    return generator.builder().CreateGlobalStringPtr(str_);
  }

  std::string name() override { return "string"; }
  void walk(ASTWalker &w) override { w.attr("str", str_); }
  size_t length() const { return str_.length(); }

 private:
  std::string str_;
};

class FunctionType : public Type {
 public:
  Ptr<Type> ret;
  std::weak_ptr<Class> parent;
  std::vector<Ptr<Type>> args;
  virtual std::string name() override {
    return "function_type";
  }
  void walk(ASTWalker &w) override
  {
    //w.attr("id", text());
  }

  void check() override {
    assert(0);
  }

  virtual bool equals(Ptr<Type> t) override {
    auto f = std::dynamic_pointer_cast<FunctionType>(t);
    if (!::equals(ret, f->ret))
      return false;

    return std::equal(args.begin(), args.end(),
                      f->args.begin(), f->args.end(),
                      [](auto x, auto y) { return ::equals(x, y); });
  }

  Value *codegen(CodeGenerator& generator) override;
};

class ReferenceType : public Type {
 public:
  Ptr<Type> type;
  void walk(ASTWalker &w) override
  {
    //w.attr("id", text());
  }

  void check() override {
    assert(0);
  }

  ReferenceType(Ptr<Type> type) : type(type) {
  }

  virtual bool equals(Ptr<Type> t) override {
    auto ref = CastPointer<ReferenceType>(t);
    return ::equals(type, ref->type);
  }

  Value *codegen(CodeGenerator& generator) override {
    if (!hasValue()) {
      type->codegen(generator);
      value(llvm::PointerType::get(type->value(), 0));
    }
    return nullptr;
  }
};

class IncompleteType : public Type {
  virtual std::string name() override {
    return "incomplete";
  }

  void walk(ASTWalker &w) override
  {
    //w.attr("id", text());
  }

  virtual bool complete() override { return false; }

  void check() override {
    assert(0);
  }

  virtual bool equals(Ptr<Type> t) override {
    return false;
  }
};

class Array : public Node<Type, Integer, length, Type, type> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "array";
  }

  virtual bool equals(Ptr<Type> t) override {
    auto a = std::dynamic_pointer_cast<Array>(t);
    return get<length>()->val() == a->get<length>()->val() &&
           ::equals(get<type>(), a->get<type>());
  }

  virtual bool complete() override { return checked_ >= 2; }

  void check() override {
    if (checked_) return;
    checked_ = 1;
    if (get<length>()->val() < 0)
      throw SementicError(get<length>(), "array length may not be negative.");

    assert(get<type>().get() != this);

    get<type>()->check();

    if (!get<type>()->complete())
      throw SementicError(get<type>(), "array element type is incomplete.");

    checked_ = 2;
  }

  Value *codegen(CodeGenerator& generator) override {
    get<type>()->codegen(generator);
    if (!hasValue())
      value(llvm::ArrayType::get(get<type>()->value(), get<length>()->val()));
    return nullptr;
  }

 private:
  int checked_{};
};

class UserType : public Node<Type, Identifier, id> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "typeref";
  }

  virtual bool equals(Ptr<Type> t) override {
    throw std::runtime_error("UserType should be dereferenced.");
  }
  
  virtual bool complete() override { return type ? type->complete() : false; }

  Value *codegen(CodeGenerator& generator) override {
    if (!hasValue()) {
      type->codegen(generator);
      assert(!hasValue());
      value(type->value());
    }
    return nullptr;
  }

  void check() override;
  Ptr<Type> type;
};

inline Ptr<Type> dereference(Ptr<Type> t) {
  while (auto u = std::dynamic_pointer_cast<UserType>(t))
    t = u->type ? u->type : std::make_shared<IncompleteType>();
  return t;
}

inline bool equals(Ptr<Type> lhs, Ptr<Type> rhs) {
  if (lhs == nullptr)
    return rhs == nullptr;

  else if (rhs == nullptr)
    return false;

  lhs = dereference(lhs);
  rhs = dereference(rhs);
  if (typeid(*lhs) != typeid(*rhs))
    return false;

  return lhs->equals(rhs);
}

class Class : public Node<Type,
    UserType, extends,
    List<Declaration>, decls> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "class";
  }

  virtual bool equals(Ptr<Type> t) override {
    return this == t.get();
  }

  std::weak_ptr<Class> self;

  virtual bool complete() override { return checked_ >= 2; }

  void check() {
    if (checked_) return;
    checked_ = 1;
    if (get<extends>()) {
      get<extends>()->check();

      if (!get<extends>()->complete())
        throw TypeError(get<extends>(), "base class is incomplete.");

      if (typeid(*dereference(get<extends>())) != typeid(Class))
        throw TypeError(get<extends>(), "base should be class");
    }

    auto sc = Scope::new_scope(scope());
    for (auto&& decl : *get<decls>()) {
      decl->parent = self;
      decl->check();
      if (!CastPointer<Type>(decl->get("type"))->complete())
        throw SementicError(decl, "Class cannot contains a member of incomplete type.");
    }

    checked_ = 2;
  }

  Value *codegen(CodeGenerator& generator) override;

  Ptr<ClassScope> scope() {
    if (scope_)
      return scope_;

    std::shared_ptr<Class> super = CastPointer<Class>(dereference(get<extends>()));
    assert(!equals(super));
    scope_ = std::make_shared<ClassScope>(super ? super->scope() : nullptr);
    for (auto&& d : *get<decls>())
      scope_->add(d);

    return scope_;
  }

 private:
  int checked_{};
  Ptr<ClassScope> scope_;
};

class TypeDeclaration : public Node<Declaration,
    Identifier, id,
    Type, type> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "type";
  }

  void check() override {
    get<type>()->check();
  }
  
  Value *value() override {
    assert(0);
    return reinterpret_cast<Value *>(get<type>()->value());
  }

  Value *codegen(CodeGenerator& generator) override {
    return get<type>()->codegen(generator);
  }
};

class VariableDeclaration : public Node<Declaration,
    Identifier, id,
    Type, type> {
 public:
  using Node::Node;

  bool is_ref{};

  virtual std::string name() override {
    return "var";
  }

  void check() override {
    get<type>()->check();
  }

  Value *value() override {
    assert(value_);
    return value_;
  }

  int index() const { return id_; }

  Value *codegen(CodeGenerator& generator) override {
    assert(!value_);
    assert(id_ == -1);
    assert(!is_ref);
    get<type>()->codegen(generator);
    return value_ = generator.builder().CreateAlloca(
        get<type>()->value(), nullptr, get<id>()->text()
    );
  }

  Value *codegen(CodeGenerator& generator, Value *v) {
    assert(!value_);
    assert(id_ == -1);
    get<type>()->codegen(generator);
    if (is_ref) {
      value_ = v;
    } else {
      generator.builder().CreateStore(v, codegen(generator));
    }
    return value_;
  }

  Value *codegen(CodeGenerator& generator, int id) {
    assert(!value_);
    assert(id_ == -1);
    get<type>()->codegen(generator);
    id_ = id;
    return nullptr;
  }

 private:
  int id_{-1};
  Value *value_{};
};

class Block : public Node<ASTNode,
    List<VariableDeclaration>, vars,
    List<Statement>, statements> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "block";
  }

  void check() {
    auto scope = Scope::current();
    for (auto&& d : *get<vars>()) {
      scope->add(d);
      d->check();
    }
    get<statements>()->check();
  }

  Value *codegen(CodeGenerator& generator) override {
    for (auto&& var : *get<vars>())
      var->codegen(generator);

    for (auto&& stat : *get<statements>())
      stat->codegen(generator);

    return nullptr;
  }
};

class Function : public Node<Declaration,
    Identifier, id,
    List<VariableDeclaration>, args,
    Type, returns,
    Block, block> {
  Ptr<FunctionType> ftype;
 public:
  // External function
  Function(std::string id,
           std::vector<Ptr<Type>> args,
           Ptr<Type> returns)
    : Node(std::make_shared<Identifier>(id), nullptr, returns, nullptr) {
    
    auto t = std::make_shared<FunctionType>();
    t->ret = returns;
    t->args = std::move(args);
    t->parent = std::shared_ptr<class Class>();

    ftype = t;
  }

  Function(CastPointer<Identifier> id,
           CastPointer<List<Identifier>> args,
           CastPointer<List<VariableDeclaration>> vars,
           CastPointer<Type> returns,
           CastPointer<Block> block,
           CastPointer<Identifier> id2) : Node(id, vars, returns, block) {
    if (id->text() != id2->text())
      throw std::invalid_argument(
          "identifer at function end not match that at beginning.");

    using Id = struct id;

    auto c = mismatch(args->begin(), args->end(),
                      vars->begin(), vars->end(),
                      [](auto x, Ptr<VariableDeclaration> y) {
                        return x->text() == y->get<Id>()->text();
                      });
    auto u = c.first;
    auto v = c.second;
    auto x = u == args->end(), y = v == vars->end();
    if (!x) {
      if (y)
        throw std::invalid_argument(
            "expect variable declaration of argument '" + (*u)->text() + "'");
      else
        throw std::invalid_argument(
            "expect variable declaration of argument '" + (*u)->text() + "', "
            "but got '" + (*v)->get<Id>()->text() + "'");
    } else if (!y) {
        throw std::invalid_argument(
            "unexpected argument variable declaration of '" +
            (*v)->get<Id>()->text() + "'");
    }
    auto i = args->begin();
    auto j = vars->begin();
    for (; i != args->end(); ++i, ++j) {
      (*j)->is_ref = typeid(**i) == typeid(RefIdentifer);
    }
  }

  virtual std::string name() override {
    return "function";
  }

  Value *value() override {
    assert(value_);
    return value_;
  }

  Value *codegen(CodeGenerator& generator) override {
    if (value_) return value_;

    type()->codegen(generator);
    
    if (!get<block>()) 
      return value_ = llvm::Function::Create(static_cast<llvm::FunctionType*>(type()->value()),
        llvm::Function::ExternalLinkage, get<id>()->text(), generator.module());

    value_ = llvm::Function::Create(static_cast<llvm::FunctionType*>(type()->value()),
        llvm::Function::InternalLinkage, "", generator.module());

    auto blk = llvm::BasicBlock::Create(generator.context(), "", value_);
    generator.builder().SetInsertPoint(blk);

    auto f = value_->arg_begin();
    if (auto p = parent.lock())
      self = f++;
    
    for (auto&& d : *get<args>()) {
      assert(f != value_->arg_end());
      d->codegen(generator, f++);
    }

    assert(f == value_->arg_end());

    Value *ret = get<returns>() ?
      generator.builder().CreateAlloca(get<returns>()->value(), nullptr, "return") :
      nullptr;

    get<block>()->codegen(generator);

    if (ret)
      generator.builder().CreateRet(generator.builder().CreateLoad(ret));
    else
      generator.builder().CreateRetVoid();
    return value_;
  }

  Ptr<FunctionType> type() {
    if (ftype) {
      assert(ftype->parent.lock() == parent.lock());
      return ftype;
    }

    std::vector<Ptr<Type>> types;
    std::transform(get<args>()->begin(), get<args>()->end(),
        std::back_inserter(types), [](auto x) {
        auto t = x->template get<::type>();
        return x->is_ref ? std::make_shared<ReferenceType>(t): t;
    });
    auto t = std::make_shared<FunctionType>();
    t->ret = get<returns>();
    t->args = std::move(types);
    t->parent = parent;

    return ftype = t;
  }

  void check() override {
    auto ret = get<returns>();
    auto scope = FunctionScope::new_scope(ret, this);
    for (auto&& a : *get<args>()) {
      scope->add(a);
      a->check();
    }
    if (ret)
      ret->check();
    if (get<block>())
      get<block>()->check();
  }

  using Node::get;
  NodePtr get(std::string s) override {
    if (s == "type") return type();
    return Node::get(std::move(s));
  }

  Value *self;

 private:
  llvm::Function *value_;
};

class NamedVariable : public Node<Variable, Identifier, id> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "variable";
  }

  Ptr<Declaration> declaration() {
    if (!declaration_) {
      declaration_ = Scope::current()->lookup(get<id>()->text());
      if (!declaration_)
        throw SementicError(location, "Undecalared symbol name '" + get<id>()->text() + "'.");

      std::type_index t = typeid(*declaration_);
      if (t == typeid(TypeDeclaration))
        throw TypeError(location, "unexpected type.");

      assert(t == typeid(VariableDeclaration) ||
             t == typeid(Function));

      if (auto fscope = FunctionScope::current())
        function_ = fscope->function();
    }

    return declaration_;
  }

  Ptr<Type> type() override {
    return CastPointer<Type>(declaration()->get("type"));
  }

  Value* object() { return function_->self; }

  Value* codegen(CodeGenerator& generator) override {
    auto d = declaration();
    if (auto v = std::dynamic_pointer_cast<VariableDeclaration>(d)) {
      if (auto c = d->parent.lock()) {
        auto p = generator.builder().CreatePointerCast(function_->self, llvm::PointerType::get(c->value(), 0));
        return generator.builder().CreateStructGEP(p, v->index());
      }
    } else {
      d->codegen(generator);
    }

    return d->value();
  }
  
 private:
  Function* function_;
  Ptr<Declaration> declaration_;
};

class LoadExpression : public Node<Expression, Variable, var> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "load";
  }

  Ptr<Type> type() override {
    return get<var>()->type();
  }

  Value* codegen(CodeGenerator& generator) override {
    return generator.builder().CreateLoad(get<var>()->codegen(generator));
  }
};

class Reference : public Node<Expression, Variable, var> {
  using Node::Node;

  virtual std::string name() override {
    return "ref";
  }
  
  Ptr<Type> type() override {
    return std::make_shared<ReferenceType>(get<var>()->type());
  }


  Value* codegen(CodeGenerator& generator) override {
    return get<var>()->codegen(generator);
  }

};


class ArrayIndex : public Node<Variable,
    Variable, var,
    Expression, struct index> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "at";
  }

  virtual Ptr<Type> type() override {
    static auto itype = std::make_shared<BuiltinType>(kInteger);
    if (!equals(get<struct index>()->type(), itype))
      throw TypeError(get<struct index>(), "array index should be an integer.");
    auto atype = std::dynamic_pointer_cast<Array>(dereference(get<var>()->type()));
    if (!atype) {
      throw TypeError(get<var>(), "left operand of subscript operator is not an array.");
	}
    return atype->get<struct type>();
  }

  Value *codegen(CodeGenerator& generator) override {
    auto v = get<var>()->codegen(generator);
    auto i = get<struct index>()->codegen(generator);
    std::vector<Value *> x { Integer(0).codegen(generator), i };
    return generator.builder().CreateInBoundsGEP(v, x);
  }
};

class FunctionCall : public Node<Expression,
    Variable, function,
    List<Expression>, args> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "call";
  }

  virtual Ptr<Type> type() override {
    auto ftype = std::dynamic_pointer_cast<FunctionType>(
        dereference(get<function>()->type())
      );
    if (!ftype)
      throw TypeError(get<function>(), "ERROR: Calling non-function object.");

    if (!equal(ftype->args.begin(), ftype->args.end(),
               get<args>()->begin(), get<args>()->end(),
               [](auto x, auto y) { return equals(x, y->type()); }))
      throw TypeError(location, "Calling arguments not matching the decalared one.");

    return ftype->ret;
  }

  Value *codegen(CodeGenerator& generator) override {
    std::vector<Value *> a;
    auto ftype = CastPointer<FunctionType>(
        dereference(get<function>()->type())
      );
    
    auto f = get<function>()->codegen(generator);
    if (auto c = ftype->parent.lock()) {
      auto p = generator.builder().CreatePointerCast(get<function>()->object(), llvm::PointerType::get(c->value(), 0));
      a.push_back(p);
    }

    transform(get<args>()->begin(), get<args>()->end(), back_inserter(a),
        [&generator](auto x) { return x->codegen(generator); });

    return generator.builder().CreateCall(f, a);
  }
};

class CallStatement : public Node<Statement,
    FunctionCall, expr> {
 public:
  using Node::Node;

  void check() override {
    // TypeError in type()
    // ignores what actual type is
    get<expr>()->type();
  }

  Value *codegen(CodeGenerator& generator) override {
    return get<expr>()->codegen(generator);
  }
};

class MemberAccess : public Node<Variable,
    Variable, var,
    Identifier, id> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "member";
  }

  virtual Ptr<Type> type() override {
    auto ctype = std::dynamic_pointer_cast<Class>(
        dereference(get<var>()->type())
      );
    if (!ctype)
      throw TypeError(get<var>(), "ERROR: trying to get member of non-class object.");

    declaration_ = ctype->scope()->lookup(get<id>()->text());
    if (!declaration_)
      throw SementicError(get<id>(), "Undeclared symbol");
    return CastPointer<Type>(declaration_->get("type"));
  }

  Value *codegen(CodeGenerator& generator) override {
    object_ = get<var>()->codegen(generator);
    if (auto d = std::dynamic_pointer_cast<VariableDeclaration>(declaration_)) {
      auto c = d->parent.lock();
      auto p = generator.builder().CreatePointerCast(object_,
          llvm::PointerType::get(c->value(), 0));

      return generator.builder().CreateStructGEP(p, d->index());
    }
    return declaration_->codegen(generator);
  }

  Value *object() { assert(object_); return object_; }

 private:
  Value *object_;
  Ptr<Declaration> declaration_;
};

class BooleanConstant : public Expression {
 public:
  BooleanConstant(bool value) : value_(value) { }
  virtual std::string name() override {
    return "boolean";
  }
  void walk(ASTWalker &w) override
  {
    w.attr("value", text());
  }
  std::string text() {
    return value_ ? "yes" : "no";
  }
  //NodeMode mode() override { return NodeMode::String; }
  Ptr<Type> type() override {
    static auto btype = std::make_shared<BuiltinType>(kBoolean);
    return btype;
  }

  Value *codegen(CodeGenerator& generator) {
    return llvm::ConstantInt::get(generator.context(), llvm::APInt(1, value_));
  }
 private:
  bool value_;
};

class PrintStatement: public Node<Statement, Expression, expr> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "print";
  }

  void check() override {
    auto type = dereference(get<expr>()->type());
    if (typeid(*type) != typeid(BuiltinType))
      throw TypeError(get<expr>(), "operand of print should be either an integer or a boolean value");
  }

  Value *codegen(CodeGenerator& generator) override {
    int bits = CastPointer<BuiltinType>(dereference(get<expr>()->type()))->bits();
    auto FT = llvm::FunctionType::get(llvm::Type::getVoidTy(generator.context()),
        {llvm::Type::getIntNTy(generator.context(), bits)}, false);
    auto F = generator.module()->getOrInsertFunction("print" + std::to_string(bits), FT);
    return generator.builder().CreateCall(F, get<expr>()->codegen(generator));
  }
};

class PrintString: public Node<Statement, String, str> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "print";
  }

  void check() override {
  }

  Value *codegen(CodeGenerator& generator) override {
    auto value = get<str>()->codegen(generator);
    auto SizeT = llvm::Type::getIntNTy(generator.context(), sizeof(size_t) * 8);
    auto FT = llvm::FunctionType::get(llvm::Type::getVoidTy(generator.context()),
        std::vector<llvm::Type *>{value->getType(), SizeT}, false);

    auto F = generator.module()->getOrInsertFunction("printstr", FT);
    return generator.builder().CreateCall2(F, value,
        llvm::ConstantInt::get(SizeT, get<str>()->length()));
  }
};

class PrintLine : public PrintString {
 public:
  PrintLine() : PrintString(std::make_shared<String>("\n")) { }
};

class AssignStatement: public Node<Statement,
    Variable, var,
    Expression, expr> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "assign";
  }

  void check() override {
    if (!equals(get<var>()->type(), get<expr>()->type()))
      throw TypeError(location, "both operand of assign operator should have same type.");

    if (auto p = std::dynamic_pointer_cast<FunctionType>(get<var>()->type()))
      throw TypeError(location, "function cannot be assigned");
  }

  Value *codegen(CodeGenerator& generator) override {
    auto v = get<var>()->codegen(generator);
    auto e = get<expr>()->codegen(generator);
    return generator.builder().CreateStore(e, v);
  }
};

class IfStatement : public Node<Statement,
    Expression, cond,
    List<Statement>, yes,
    Statement, no> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "if";
  }

  void check() override {
    static auto btype = std::make_shared<BuiltinType>(kBoolean);
    if (!equals(get<cond>()->type(), btype))
      throw TypeError(get<cond>(), "condition should have boolean type.");
    get<yes>()->check();
    if (get<no>())
      get<no>()->check();
  }

  Value *codegen(CodeGenerator& generator) override {
    auto thenb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto elseb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto nextb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());

    //thenb->moveAfter(generator.builder().GetInsertBlock());
    //elseb->moveAfter(thenb);
    //nextb->moveAfter(elseb);
    generator.builder().CreateCondBr(get<cond>()->codegen(generator), thenb, elseb);

    // then block
    generator.builder().SetInsertPoint(thenb);
    get<yes>()->codegen(generator);
    generator.builder().CreateBr(nextb);

    // else block
    generator.builder().SetInsertPoint(elseb);
    if (get<no>())
      get<no>()->codegen(generator);
    generator.builder().CreateBr(nextb);

    // end if
    generator.builder().SetInsertPoint(nextb);
    return nullptr;
  }
};

class ForeachStatement : public Node<Statement,
    Variable, var,
    Expression, expr,
    List<Statement>, statements> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "foreach";
  }

  void check() override {
    auto atype = std::dynamic_pointer_cast<Array>(dereference(get<expr>()->type()));
    if (!atype)
      throw TypeError(get<expr>(), "for each in non-array object.");
    if (!equals(get<var>()->type(), atype->get<type>()))
      throw TypeError(get<var>(), "element type not match the array type.");
    get<statements>()->check();
  }

  Value *codegen(CodeGenerator& generator) override {
    auto loopb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto dob = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto nextb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto current = generator.builder().GetInsertBlock();
    auto array = get<expr>()->codegen(generator);

    generator.builder().CreateBr(loopb);

    // loop block
    generator.builder().SetInsertPoint(loopb);
    auto PN = generator.builder().CreatePHI(llvm::Type::getInt32Ty(generator.context()), 2);
    auto atype = CastPointer<Array>(dereference(get<expr>()->type()));
    PN->addIncoming(Integer(0).codegen(generator), current);
    auto cond = generator.builder().CreateICmpULT(PN, atype->get<length>()->codegen(generator));
    generator.builder().CreateCondBr(cond, dob, nextb);

    // do block
    generator.builder().SetInsertPoint(dob);
    std::vector<Value *> x { Integer(0).codegen(generator), PN };
    auto ptr = generator.builder().CreateInBoundsGEP(array, x);
    auto value = generator.builder().CreateLoad(ptr);
    generator.builder().CreateStore(value, get<var>()->codegen(generator));
    get<statements>()->codegen(generator);
    auto nexti = generator.builder().CreateAdd(PN, Integer(1).codegen(generator));
    PN->addIncoming(nexti, generator.builder().GetInsertBlock());
    generator.builder().CreateBr(loopb);

    // end foreach
    generator.builder().SetInsertPoint(nextb);
    return nullptr;
  }
};

class ForStatement : public Node<Statement,
    Variable, var,
    Expression, from,
    Integer, delta,
    Expression, to,
    List<Statement>, statements> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "for";
  }

  void check() override {
    static auto itype = std::make_shared<BuiltinType>(kInteger);
    if (!equals(get<var>()->type(), itype))
      throw TypeError(get<var>(), "Loop variable should be an integer");
    if (!equals(get<from>()->type(), itype))
      throw TypeError(get<var>(), "Type of loop range should match the loop variable");
    if (!equals(get<to>()->type(), itype))
      throw TypeError(get<var>(), "Type of loop range should match the loop variable");
    get<statements>()->check();
  }

  Value *codegen(CodeGenerator& generator) override {
    auto loopb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto dob = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto nextb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto current = generator.builder().GetInsertBlock();

    auto F = get<from>()->codegen(generator);
    auto D = get<delta>()->codegen(generator);
    auto T = get<to>()->codegen(generator);
    generator.builder().CreateBr(loopb);

    // loop block
    generator.builder().SetInsertPoint(loopb);
    auto PN = generator.builder().CreatePHI(llvm::Type::getInt32Ty(generator.context()), 2);
    PN->addIncoming(F, current);
    auto cond = get<delta>()->val() > 0 ? generator.builder().CreateICmpSLE(PN, T) : generator.builder().CreateICmpSGE(PN, T);
    generator.builder().CreateCondBr(cond, dob, nextb);

    // do block
    generator.builder().SetInsertPoint(dob);
    generator.builder().CreateStore(PN, get<var>()->codegen(generator));
    get<statements>()->codegen(generator);
    auto nexti = generator.builder().CreateAdd(PN, D);
    PN->addIncoming(nexti, generator.builder().GetInsertBlock());
    generator.builder().CreateBr(loopb);

    // end for
    generator.builder().SetInsertPoint(nextb);
    return nullptr;
  }
};
class WhileStatment : public Node<Statement,
    Expression, cond,
    List<Statement>, statements> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "while";
  }

  void check() override {
    static auto btype = std::make_shared<BuiltinType>(kBoolean);
    if (!equals(get<cond>()->type(), btype))
      throw TypeError(get<cond>(), "condition should have boolean type.");
    get<statements>()->check();
  }

  Value *codegen(CodeGenerator& generator) override {
    auto condb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto dob = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto nextb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());

    generator.builder().CreateBr(condb);

    // cond block
    generator.builder().SetInsertPoint(condb);
    generator.builder().CreateCondBr(get<cond>()->codegen(generator), dob, nextb);

    // do block
    generator.builder().SetInsertPoint(dob);
    get<statements>()->codegen(generator);
    generator.builder().CreateBr(condb);

    // end while
    generator.builder().SetInsertPoint(nextb);
    return nullptr;
  }
};

class RepeatStatment : public Node<Statement,
    Expression, cond,
    List<Statement>, statements> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "repeat";
  }

  void check() override {
    static auto btype = std::make_shared<BuiltinType>(kBoolean);
    if (!equals(get<cond>()->type(), btype))
      throw TypeError(get<cond>(), "condition should have boolean type.");
    get<statements>()->check();
  }

  Value *codegen(CodeGenerator& generator) override {
    auto dob = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    auto nextb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());

    generator.builder().CreateBr(dob);

    // do block
    generator.builder().SetInsertPoint(dob);
    get<statements>()->codegen(generator);
    generator.builder().CreateCondBr(get<cond>()->codegen(generator), nextb, dob);

    // end repeat
    generator.builder().SetInsertPoint(nextb);
    return nullptr;
  }
};

class ReturnStatment : public Node<Statement, Expression, returns> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "return";
  }

  void check() override {
    if (!equals(FunctionScope::current()->returns(), get<returns>() ? get<returns>()->type() : nullptr))
      throw TypeError(get<returns>(), "value returning is not matching the declared one.");
  }

  Value *codegen(CodeGenerator& generator) override {
    if (get<returns>())
      generator.builder().CreateRet(get<returns>()->codegen(generator));
    else
      generator.builder().CreateRetVoid();

    auto nextb = llvm::BasicBlock::Create(generator.context(), "", generator.builder().GetInsertBlock()->getParent());
    generator.builder().SetInsertPoint(nextb);
    return nullptr;
  }
};

class Program : public Node<ASTNode,
    Identifier, id,
    List<Declaration>, decls,
    Block, block> {
 public:
  using Node::Node;
  virtual std::string name() override {
    return "program";
  }

  void check() {
    auto scope = Scope::new_scope();
    scope->add(std::make_shared<Function>("input", std::vector<Ptr<Type>>{}, std::make_shared<BuiltinType>(kInteger)));
    scope->add(std::make_shared<Function>("ask", std::vector<Ptr<Type>>{}, std::make_shared<BuiltinType>(kBoolean)));
    for (auto&& d : *get<decls>()) {
      scope->add(d);
      d->check();
    }
    get<block>()->check();
  }

  using Node::codegen;

  Ptr<CodeGenerator> codegen() {
    auto generator = std::make_shared<CodeGenerator>(get<id>()->text());
    for (auto&& decl : *get<decls>())
      decl->codegen(*generator);

    auto FT = llvm::FunctionType::get(llvm::Type::getVoidTy(generator->context()), false);
    auto F = llvm::Function::Create(FT, llvm::Function::ExternalLinkage, "main", generator->module());

    auto blk = llvm::BasicBlock::Create(generator->context(), "", F);
    generator->builder().SetInsertPoint(blk);
    get<block>()->codegen(*generator);
    generator->builder().CreateRetVoid();
    return generator;
  }
};

inline void Scope::add(std::string s, Ptr<Declaration> d) {
  std::cerr << "Add symbol " << s << std::endl;
  if (!table_.emplace(s, d).second) {
    throw SementicError(d, "Duplicated symbol name '" + s + "'.");
  }
}

inline void Scope::add(Ptr<Declaration> d) {
  return add(CastPointer<Identifier>(d->get("id"))->text(), d);
}

inline void UserType::check() {
  //assert(!type);
  if (!type) {
    auto decl = Scope::current()->lookup(get<id>()->text());
    if (!decl)
      throw SementicError(location, "Undecalared symbol name '" + get<id>()->text() + "'.");
    else if (auto t = std::dynamic_pointer_cast<class TypeDeclaration>(decl))
      type = t->get<::type>();
    else
      throw TypeError(location, "expect type, got " + decl->name());

    if (type.get() == this)
      throw SementicError(location, "may not decalare a type with itself.");
  }
}

inline llvm::Value *Class::codegen(CodeGenerator& generator) {
  if (hasValue())
    return nullptr;

  auto c = llvm::StructType::create(generator.context());
  value(c);

  std::vector<llvm::Type *> members;
  members.reserve(get<decls>()->size() + 1);
  if (get<extends>()) {
    get<extends>()->codegen(generator);
    members.push_back(get<extends>()->value());
  }

  for (auto&& decl : *get<decls>()) {
    if (auto var = std::dynamic_pointer_cast<VariableDeclaration>(decl)) {
      var->codegen(generator, members.size());
      members.push_back(var->get<type>()->value());
    }
  }

  c->setBody(members);

  for (auto&& decl : *get<decls>()) {
    if (auto f = std::dynamic_pointer_cast<Function>(decl))
      f->codegen(generator);
  }

  return nullptr;
}

inline llvm::Value *FunctionType::codegen(CodeGenerator& generator) {
  if (ret)
    ret->codegen(generator);
  for (auto&& t : args)
    t->codegen(generator);
  std::vector<llvm::Type *> types;
  if (auto p = parent.lock())
    types.push_back(llvm::PointerType::get(p->value(), 0));
  transform(args.begin(), args.end(), back_inserter(types), [](auto x) { return x->value(); });
  value(llvm::FunctionType::get(ret ? ret->value() : llvm::Type::getVoidTy(generator.context()), std::move(types), false));
  return nullptr;
}
#endif  // AST_H_
