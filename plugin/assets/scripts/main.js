
function init() {
  var input = $("#textarea_input");

  function out(data) {
    if (typeof data.data === 'string' && data.result === "ok") {
      $("#json").text(data.data);
    } else {
      $("#json").JSONView(data);
    }
  }

  $("#btn_lexical").click(function() {
    chrome.runtime.sendNativeMessage('cn.edu.sjtu.se.mylang',
      { option: "-l", code: input.val() }, function(response) {
        out(response);
    });
  });

  $("#btn_syntax").click(function() {
    chrome.runtime.sendNativeMessage('cn.edu.sjtu.se.mylang',
      { option: "-g", code: input.val() }, function(response) {
        out(response);
    });
  });
  
  $("#btn_sementic").click(function() {
    chrome.runtime.sendNativeMessage('cn.edu.sjtu.se.mylang',
      { option: "-s", code: input.val() }, function(response) {
        out(response);
    });
  });
  
  $("#btn_assembly").click(function() {
    chrome.runtime.sendNativeMessage('cn.edu.sjtu.se.mylang',
      { option: "-a", code: input.val() }, function(response) {
        out(response);
    });
  });

  input.val('helloworld');
}

$(document).ready(function() {
  init();
});
