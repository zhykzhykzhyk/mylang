#include <stdio.h>
#include <string.h>
#include <fstream>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <sys/mman.h>
#include <fcntl.h>

using namespace std;

int writeall(int fd, const void *buf, size_t count)
{
  size_t pos = 0;
  const char *cbuf = static_cast<const char *>(buf);
  while (pos < count) {
    switch (int result = write(fd, cbuf + pos, count - pos)) {
      case EINTR:
        continue;

      case EAGAIN:
        if (pos != 0) return pos;
        return result;

      default:
        if (result < 0) return result;
        pos += result;
    }
  }
  return pos;
}

#define CHECK(f) ({ auto r = f; if ((intptr_t)r < 0) perror(#f); r; })

int main(int argc, char *argv[])
{
  int len;
  while (fread(&len, 4, 1, stdin) == 1) {
    fprintf(stderr, "ACCEPT\n");
    char data[len];
    CHECK(fread(data, len, 1, stdin));
    char in[] = "/tmp/mlinXXXXXX";
    char out[] = "/tmp/mloutXXXXXX";
    int fdin = CHECK(mkstemp(in));
    CHECK(writeall(fdin, data, len));
    CHECK(close(fdin));
    int fdout = CHECK(mkstemp(out));
    if (pid_t pid = fork()) {
      if (pid < 0) {
        CHECK(close(fdout));
        continue;
      }
      CHECK(wait(NULL));
      struct stat status;
      CHECK(fstat(fdout, &status));
      int size = status.st_size;
      CHECK(write(1, &size, 4));
      void *data = CHECK(mmap(
            NULL, size, PROT_READ, MAP_PRIVATE | MAP_NORESERVE,
            fdout, 0));
      CHECK(writeall(1, data, size));
      CHECK(munmap(data, size));
    } else {
      CHECK(close(fdout));
      CHECK(chdir(PREFIX "/bin"));
      CHECK(execl("./mlcchost.sh", "./mlcchost.sh", in, out, NULL));
      exit(1);
    }
  }
}
