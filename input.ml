program input()
is
  var a is integer;
  var b is boolean;
begin
  a := input();
  b := ask();
  println a;
  println b;
end
