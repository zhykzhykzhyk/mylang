MyLang 编译器
======

* 小组成员
  * 傅浩南 学号:5120379109
  * 张元凯 学号:5120379015
* 语言: C/C++
* 工具
  * bison
  * flex
  * Clang
  * llvm
  * Chromium
* 实现功能
  * 基本功能全部实现(示例代码中所用到的所有语言特性)
  * 拓展功能
    * 短路逻辑
    * 引用传参
    * 用户输入
    * 文本输出
    * FOR语句
* 使用说明
  * 使用说明
    * `README.md`
  * 依赖
    * llvm >= 3.5
    * bison
    * flex
    * Clang
  * 安装
    * `make install`
  * 使用
    * `mlcc [-o <output>] input.ml`
  * GUI
    * 依赖: Chromium
    * 系统: Ubuntu
    * 运行: `make run`
