#include "mylang.tab.hh"
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <llvm/Support/raw_ostream.h>

enum {
  Lex,
  Gramma,
  Sementic,
  Assembly,
  Executable,
} syntax;
bool json;

std::shared_ptr<Program> yytree;

using namespace std::literals;

Ptr<Scope> Scope::current_;

class TypedVariable : public Node<ASTNode,
  Identifier, id,
  Type, type> {
    using Node::Node;
  };

class VariablePrinter : ASTWalker {
  NodePtr list;
public:
  VariablePrinter(decltype(list) l) : list(l) { }

  virtual void attr(std::string name, std::string value) { }
  virtual void attr(std::string name, int value) { }

  virtual void node(std::string name, NodePtr node) {
    if (auto b = std::dynamic_pointer_cast<Block>(node)) {
      auto l = std::make_shared<List<TypedVariable>>();
      for (auto&& v : *b->get<vars>()) {
        l->add(ListBack, std::make_shared<TypedVariable>(
              v->get<id>(),
              dereference(v->get<type>())));
      }
      list->add(ListBack, l);
    } else if (node) {
      node->walk(*this);
    }
  }

};

static struct Result : public ASTNode {
  std::string result;
  std::shared_ptr<ASTNode> data;
  std::string sdata;

  void walk(ASTWalker &w) override {
    w.attr("result", result);
    if (data)
      w.node("data", data);
    else
      w.attr("data", sdata);
  }
} r;

void yyerror(const char *s)
{
  if (r.sdata == "") {
    using namespace std::literals;
    r.sdata = "Line " + std::to_string(yylloc.first_line) + ": " + s;
  }
}

int main(int argc, char *argv[])
{
  int opt;
  std::string output = "a.out";
  syntax = Executable;
  while ((opt = getopt(argc, argv, "vjlgaso:")) != -1)
    switch (opt) {
      case 'v':
        yydebug = 1;
        break;
      case 'j':
        json = 1;
        break;
      case 'l':
        syntax = Lex;
        break;
      case 'g':
        syntax = Gramma;
        break;
      case 's':
        syntax = Sementic;
        break;
      case 'a':
        syntax = Assembly;
        break;
      case 'o':
        output = optarg;
        break;
    }
  if (optind < argc && argv[optind] != "-"s) {
    int fd = open(argv[optind], O_RDONLY);
    dup2(fd, 0);
    close(fd);
  }
  int tok;
  switch (syntax) {
    case Lex: {
      r.data.reset(new List<ASTNode>);
      while ((tok = yylex()) != 0) {
        if (tok == ERROR) {
          r.result = "failed";
          r.data = nullptr;
          break;
        }
        r.data->add(ListBack, yylval);
      }
      if (tok != ERROR)
        r.result = "ok";
      break;
    }
    case Gramma:
      if (yyparse() != 0) {
        r.result = "failed";
      } else {
        r.result = "ok";
        r.data = yytree;
      }
      break;
    case Sementic:
      if (yyparse() != 0) {
        r.result = "failed";
      } else {
        r.result = "ok";
        r.data = nullptr;
        try {
          yytree->check();
        } catch (SementicError& e) {
          r.result = "failed";
          r.sdata = "Line " + std::to_string(e.where().first_line) + ": " + e.what();
          break;
        }
        r.data.reset(new List<List<TypedVariable>>);
        VariablePrinter(r.data).node("", yytree);
      }
      break;
    case Assembly:
    case Executable:
      if (yyparse() != 0) {
        r.result = "failed";
      } else {
        try {
          yytree->check();
        } catch (SementicError& e) {
          r.result = "failed";
          r.sdata = "Line " + std::to_string(e.where().first_line) + ": " + e.what();
          break;
        }
        r.result = "ok";
        llvm::raw_string_ostream os(r.sdata);
        yytree->codegen()->module()->print(os, nullptr);
      }
      break;
  }
  if (syntax == Executable) {
    if (r.result == "failed") {
      std::cerr << r.sdata;
      return 1;
    }
    int fd[2];
    if (pipe(fd) < 0) {
      perror("pipe");
      return 1;
    }
    if (auto pid = fork()) {
      if (pid < 0) {
        perror("fork");
        return 1;
      }
      close(fd[0]);
      FILE *f = fdopen(fd[1], "w");
      fputs(r.sdata.c_str(), f);
      fclose(f);
      waitpid(-1, NULL, 0);
      return 0;
    }
    close(fd[1]);
    dup2(fd[0], 0);
    close(fd[0]);
    execlp("clang", "clang", "-xir", "-o", output.c_str(), "-L.", "-", "-lml", NULL);
    exit(1);
  }
  std::shared_ptr<ASTNode> rp(&r, [](auto){});
  if (json) {
    JsonPrinter(std::cout).node(rp);
  } else {
    XmlPrinter(std::cout).node("result", rp);
  }
}
