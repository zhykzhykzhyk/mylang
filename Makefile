PREFIX=$(HOME)

#CC=clang
#CFLAGS=-DPREFIX="\"$(PREFIX)\"" -DNDEBUG
#CXX=clang++
#CXXFLAGS=`llvm-config --cxxflags` --std=c++1y -fexceptions -frtti -fno-pic -Wall -DNDEBUG -g0 -DPREFIX="\"$(PREFIX)\""
#LDFLAGS=`llvm-config --ldflags`
#LDLIBS=`llvm-config --system-libs --libs core` -lstdc++

CC=gcc
CFLAGS=-DPREFIX="\"$(PREFIX)\"" -DNDEBUG
CXX=g++
CXXFLAGS=`llvm-config --cxxflags` --std=c++1y -fexceptions -frtti -fno-pic -g -Og -Wall -UNDEBUG -DPREFIX="\"$(PREFIX)\""
LDLIBS=`llvm-config --system-libs --libs core` -lstdc++
LDFLAGS=`llvm-config --ldflags`

.PHONY: all clean install run -lstdc++
.PRECIOUS: %.ll %.s

all: mlcc mlcchost libml.a

%.d : %.cc
	$(CXX) -MM -MG $(CXXFLAGS) -o $*.d $<

SRC=$(wildcard *.cc)
OBJ=$(SRC:.cc=.o)
DEP=$(OBJ:.o=.d)
YACC_OUTPUT=mylang.tab.cc mylang.tab.hh mylang.output

include $(DEP)

mlcc.o: mlcc.cc mylang.tab.hh

lex.yy.o: lex.yy.c mylang.tab.hh

lex.yy.c: mylang.l
	flex mylang.l

$(YACC_OUTPUT): mylang.yy
	bison -d mylang.yy

mlcc: mlcc.o mylang.tab.o lex.yy.o

-lstdc++:
	#touch -- -lstdc++

mlcchost: mlcchost.o

plugin.crx: plugin/*
	chromium-browser --pack-extension plugin/

install: all
	mkdir -p $(PREFIX)/bin
	cp mlcchost mlcc mlcchost.sh $(PREFIX)/bin
	PREFIX=$(PREFIX) HOSTNAME=cn.edu.sjtu.se.mylang ./install-client.sh

run: install
	chromium-browser --load-and-launch-app=$(PWD)/plugin

libml.a: library.o
	ar r libml.a library.o

example.ll: example3.ml mlcc
	./mlcc -ja < example3.ml | jq -r .data > example.ll

%.ll: %.ml mlcc
	./mlcc -ja < $< | jq -r .data > $@

%.s: %.ll
	llc $<

%: %.ml mlcc libml.a
	./mlcc $< -o $@

clean:
	rm -f -- $(OBJ) $(DEP) $(YACC_OUTPUT) lex.yy.c lex.yy.o mlcc mlcchost -lstdc++
