program qsort()
  type arr is array of 100 integer;
  function qsort(ref a, l, r)
    var a is arr;
    var l is integer;
    var r is integer;
  is
    var i is integer;
    var j is integer;
    var key is integer;
    var t is integer;
  begin
    if l < r then
      i := l;
      j := r;
      key := a[l];
      while i <= j do
        while i <= j and a[i] < key do
          i := i + 1;
        end while
        while i <= j and a[j] > key do
          j := j - 1;
        end while
        if i <= j then
          t := a[i];
          a[i] := a[j];
          a[j] := t;
          i := i + 1;
          j := j - 1;
        end if
      end while
      qsort(ref a, l, j);
      qsort(ref a, i, r);
    end if
  end function qsort;
is
  var n is integer;
  var i is integer;
  var a is arr;
begin
  print "Please input n:";
  n := input();
  for i := 0 to n-1 do
    a[i] := input();
  end for
  qsort(ref a, 0, n-1);
  for i := n-1 downto 0 do
    print a[i], "    ";
  end for
end
