#ifndef PRINTER_H_
#define PRINTER_H_
#include "ast.h"

class XmlPrinter : public ASTWalker {
 public:
  XmlPrinter(std::ostream &os) : os(os) { }

  void node(std::string name, NodePtr ptr) override {
    if (name != "")
      os << "<" << name << ">";
    if (ptr) {
      auto pname = ptr->name();
      if (pname != "")
        os << "<" << pname << ">";
      ptr->walk(*this);
      if (pname != "")
        os << "</" << pname << ">";
    }
    if (name != "")
      os << "</" << name << ">";
  }

  void attr(std::string name, std::string value) override {
    if (name != "value")
      os << "<" << name << " value=\"";
    text(value);
    if (name != "value")
      os << "\" />";
  }

  void attr(std::string name, int value) override {
    if (name != "")
      os << "<" << name << " value=\"";
    os << value;
    if (name != "")
      os << "\" />";
  }

 void text(std::string value) {
    for (char c : value) {
      switch (c) {
        case '<':
          os << "&lt;";
          break;
        case '>':
          os << "&gt;";
          break;
        case '&':
          os << "&amp;";
          break;
        case '\'':
          os << "&apos;";
          break;
        case '"':
          os << "&quot;";
          break;
        default:
          os << c;
      }
    }
  }
 private:
  std::ostream &os;
};

enum class JsonMode {
  List,
  Object,
};

class JsonPrinter : public ASTWalker {
 public:
  JsonPrinter(std::ostream &os) : os(os) { }

  void node(std::string name, NodePtr ptr) override {
    attr(name);
    assert(!modes.empty());
    assert(modes.back() == NodeMode::Node || modes.back() == NodeMode::List);
    node(ptr);
  }

  void node(NodePtr ptr) {
    if (!ptr) {
      os << "null";
      return;
    }
    modes.push_back(ptr->mode());
    switch (modes.back()) {
      case NodeMode::List:
        os << '[';
        first.push_back(true);
        ptr->walk(*this);
        os << ']';
        first.pop_back();
        break;
      case NodeMode::Node:
        os << '{';
        first.push_back(true);
        if (ptr->name() != "")
          attr("node", ptr->name());
        ptr->walk(*this);
        first.pop_back();
        os << '}';
        break;
      default:
        assert(0);
    }
    modes.pop_back();
  }

  void attr(std::string name) {
    assert(!modes.empty());
    assert(modes.back() == NodeMode::Node || modes.back() == NodeMode::List);
    if (first.back()) {
      first.back() = false;
    } else {
      os << ',';
    }
    if (name != "")
      os << '"' << name << '"' << ':';
  }

  void attr(std::string name, std::string value) override {
    attr(name);
    text(value);
  }

  void attr(std::string name, int value) override {
    attr(name);
    os << value;
  }

  void text(std::string str) {
    os << '"';
    for (char c : str) {
      switch (c) {
        case '\\':
          os << "\\\\";
          break;
        case '"':
          os << "\\\"";
          break;
        case '\n':
          os << "\\n";
          break;
        case '\t':
          os << "\\t";
          break;
        case '\b':
          os << "\\b";
          break;
        case '\f':
          os << "\\f";
          break;
        case '\r':
          os << "\\r";
          break;
        default:
          os << c;
      }
    }
    os << '"';
  }
 private:
  std::ostream &os;
  std::vector<NodeMode> modes;
  std::vector<bool> first;
};
#endif  // PRINTER_H_
