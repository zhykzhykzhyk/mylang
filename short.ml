program short()
function abort()
  return boolean;
is
var a is array of 1 integer;
begin
  println "ABORTING";
  a[10101010] := 8;
end function abort;
is
begin
  if 1 > 2 and abort() then
    println "WHAT!?";
  elif 1 < 2 or abort() then
    println "SAFE";
  else
    println "WHAT!!?";
  end if
  println 1 > 2 or 1 < 2;
  println 1 < 2 and 1 > 2;
end
