%code requires{
#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN
#endif

EXTERN int yylex(void);
EXTERN void yyerror(const char *);
}
%code provides{
EXTERN int yyid(const char *);
EXTERN int yyop(const char *);
EXTERN int yyint(const char *);
EXTERN int yystr(const char *);
extern YYLTYPE yycloc;
}
%define parse.trace true
%locations
%code requires {
#include <stdio.h>
#include <string.h>
#ifdef __cplusplus
#include <string>
#include <iostream>
#include "ast.h"
#include "printer.h"

extern std::shared_ptr<Program> yytree;
#endif
}
%code {
#define reset(p, ...) reset((yycloc = yyloc, p),## __VA_ARGS__)
}
%define api.value.type {NodePtr}

%verbose
%token-table
%define parse.error verbose
%token  INTEGER IDENT ERROR STRING
%left   "or"
%left   "and"
%left   '<' '>' "<=" ">="
%left   "==" "<>"
%left   '+' '-'
%left   '*' '/' '%'
%%
program: "program" IDENT '(' ')' decls block
       {
       yytree.reset(new Program($2, $5, $6));
       }
       ;

ref_id: IDENT
      | "ref" IDENT { $$.reset(new RefIdentifer($2)); };

ref_id_list: %empty { $$.reset(new List<Identifier>()); }
           | ref_id  { $$.reset(new List<Identifier>($1)); }
           | ref_id ',' ref_id_list { $$ = $3; $$->add(ListFront, $1); }
           ;

expr_list: %empty  { $$.reset(new List<Expression>()); }
         | expr    { $$.reset(new List<Expression>($1)); }
         | expr ',' expr_list { $$ = $3; $$->add(ListFront, $1); }
         ;

block: "is" var_decls "begin" statements "end"
     { $$.reset(new Block($2, $4)); }
     ;

decls: %empty      { $$.reset(new List<Declaration>()); }
     | decls decl  { $$->add(ListBack, $2); }
     ;

decl: function_decl
    | type_decl
    ;

function_decl: "function" IDENT '(' ref_id_list ')' var_decls 
               return_decl block "function" IDENT ';'
              {
                try {
                  $$.reset(new Function($2, $4, $6, $7, $8, $10)); 
                } catch (const std::exception &e) {
                  yylloc = @$;
                  yyerror(e.what());
                  YYERROR;
                }
              }

type_decl: "type" IDENT "is" type ';'
         { $$.reset(new TypeDeclaration($2, $4)); }
         ;

type: "boolean"  { $$.reset(new BuiltinType(kBoolean)); }
    | "integer"  { $$.reset(new BuiltinType(kInteger)); }
    | "array" "of" INTEGER type { $$.reset(new Array($3, $4)); }
    | IDENT      { $$.reset(new UserType($1)); }
    | class_decl;

cdecls: %empty         { $$.reset(new List<Declaration>()); }
      | cdecls cdecl   { $$->add(ListBack, $2); }
      ;

cdecl: function_decl
     | var_decl;

class_decl: "class" extend_decl cdecls "end" "class"
          { auto c = std::make_shared<Class>($2, $3); $$ = c; c->self = c; }
          ;

extend_decl: %empty           { $$ = nullptr; }
           | "extends" IDENT  { $$.reset(new UserType($2)); }
           ;

return_decl: %empty             { $$ = nullptr; }
           | "return" type ';'  { $$ = $2; }
           ;

var_decls: %empty   { $$.reset(new List<VariableDeclaration>()); }
         | var_decls var_decl  { $$->add(ListBack, $2); }
         ;

var_decl: "var" IDENT "is" type ';'
        { $$.reset(new VariableDeclaration($2, $4)); }
        ;

statements: %empty  { $$.reset(new List<Statement>()); }
         | statements statement { $$->add(ListBack, $2); }
         ;

statement: foreach_stat
         | for_stat
         | print_stat
         | if_stat
         | repeat_stat
         | while_stat
         | assign_stat
         | return_stat
         | call_stat;

func_call: variable '(' expr_list ')' 
         { $$.reset(new FunctionCall($1, $3)); }
         ;

foreach_stat: "foreach" variable "in" variable "do" statements "end" "foreach"
            { $$.reset(new ForeachStatement($2, $4, $6)); };

for_stat: "for" variable ":=" expr "to" expr "do" statements "end" "for"
        { $$.reset(new ForStatement($2, $4, std::make_shared<Integer>(1), $6, $8)); }
        | "for" variable ":=" expr "downto" expr "do" statements "end" "for"
        { $$.reset(new ForStatement($2, $4, std::make_shared<Integer>(-1), $6, $8)); };

while_stat: "while" expr "do" statements "end" "while"
          { $$.reset(new WhileStatment($2, $4)); };

repeat_stat: "repeat" statements "until" expr ';'
          { $$.reset(new RepeatStatment($4, $2)); };

elif_stat: "elif" if_body { $$ = $2; };

else_stat: "else" statements { $$ = $2; };

if_body: expr "then" statements elif_stat
       { $$.reset(new IfStatement($1, $3, $4)); }
       | expr "then" statements else_stat
       { $$.reset(new IfStatement($1, $3, $4)); }
       | expr "then" statements
       { $$.reset(new IfStatement($1, $3, nullptr)); }
       ;

if_stat: "if" if_body "end" "if"  { $$ = $2; };

call_stat: func_call ';'
         { $$.reset(new CallStatement($1)); };

print_expr: expr    { $$.reset(new PrintStatement($1)); }
          | STRING  { $$.reset(new PrintString($1)); };

print_list: %empty  { $$.reset(new List<Statement>()); }
          | print_expr { $$.reset(new List<Statement>($1)); }
          | print_expr ',' print_list { $$ = $3; $$->add(ListFront, $1); }
          ;

print_stat: "print" print_list ';' { $$ = $2; }
          | "println" print_list ';' { $$ = $2; $$->add(ListBack, std::make_shared<PrintLine>()); };

assign_stat: variable ":=" expr ';' 
           { $$.reset(new AssignStatement($1, $3)); }
           ;

return_stat: "return" expr ';'  { $$.reset(new ReturnStatment($2)); }
           | "return" ';'  { $$.reset(new ReturnStatment(nullptr)); }
           ;

expr: variable { $$.reset(new LoadExpression($1)); }
    | "ref" variable { $$.reset(new Reference($2)); }
    | "yes"  { $$.reset(new BooleanConstant(true)); }
    | "no"   { $$.reset(new BooleanConstant(false)); }
    | INTEGER
    | func_call
    | '(' expr ')'  { $$ = $2; }
    | expr '+' expr 
    { $$.reset(new BinaryExpression($1, Operation::Addition, $3)); }
    | expr '-' expr
    { $$.reset(new BinaryExpression($1, Operation::Subtruction, $3)); }
    | expr '*' expr
    { $$.reset(new BinaryExpression($1, Operation::Multiplication, $3)); }
    | expr '/' expr
    { $$.reset(new BinaryExpression($1, Operation::Division, $3)); }
    | expr '%' expr
    { $$.reset(new BinaryExpression($1, Operation::Modulo, $3)); }
    | expr '<' expr
    { $$.reset(new BinaryExpression($1, Operation::LessThan, $3)); }
    | expr '>' expr
    { $$.reset(new BinaryExpression($1, Operation::GreaterThan, $3)); }
    | expr "<=" expr
    { $$.reset(new BinaryExpression($1, Operation::LessThanOrEqual, $3)); }
    | expr ">=" expr
    { $$.reset(new BinaryExpression($1, Operation::GreaterThanOrEqual, $3)); }
    | expr "==" expr
    { $$.reset(new BinaryExpression($1, Operation::Equal, $3)); }
    | expr "<>" expr
    { $$.reset(new BinaryExpression($1, Operation::NotEqual, $3)); }
    | expr "and" expr
    { $$.reset(new LogicalBinaryExpression($1, Operation::And, $3)); }
    | expr "or" expr
    { $$.reset(new LogicalBinaryExpression($1, Operation::Or, $3)); }
    ;

variable: IDENT                 { $$.reset(new NamedVariable($1)); }
        | variable '[' expr ']' { $$.reset(new ArrayIndex($1, $3)); }
        | variable '.' IDENT    { $$.reset(new MemberAccess($1, $3)); }
        ;

%%
#undef reset
static inline bool is_quote(char c)
{
  return c == '\'' || c == '"';
}

int yyop(const char *s)
{
  int len = strlen(s);
  for (int i = 0; i <= YYMAXUTOK; i++) {
    const char *n = yytname[yytranslate[i]];
    if (n && is_quote(*n) && !strncmp(n + 1, s, len)
      && is_quote(n[len + 1]) && n[len + 2] == 0) {
        yylval.reset(new Operator(s));
        return i;
      }
  }
  using namespace std::literals;
  yyerror(("Unknown token: "s + s).c_str());
  return ERROR;
}

int yyid(const char *s)
{
  int len = strlen(s);
  for (int i = 0; i <= YYMAXUTOK; i++) {
    const char *n = yytname[yytranslate[i]];
    if (n && is_quote(*n) && !strncmp(n + 1, s, len)
      && is_quote(n[len + 1]) && n[len + 2] == 0) {
        yylval.reset(new Keyword(s));
        return i;
      }
  }
  yylval.reset(new Identifier(s));
  return IDENT;
}

YYLTYPE yycloc;

int yyint(const char *s)
{
  yylval.reset(new Integer(atoi(s)));
  return INTEGER;
}

int yystr(const char *s)
{
  std::string p;
  while (*++s != '"') {
    if (*s != '\\') p += *s;
    else switch (*++s) {
    case 'n':
      p += '\n';
      break;
    default:
      p += *s;
    }
  }
  assert(s[1] == '\0');
  yylval.reset(new String(p));
  return STRING;
}
