program reftest()
  function inc1(x)
    var x is integer;
  is
  begin
    x := x + 1;
  end function inc1;
  function refinc1(ref x)
    var x is integer;
  is
  begin
    x := x + 1;
  end function refinc1;
is
  var a is integer;
begin
  a := 3;
  print a;
  inc1(a);
  print a;
  refinc1(ref a);
  print a;
end
