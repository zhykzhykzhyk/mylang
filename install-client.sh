#!/bin/sh

mkdir -p $HOME/.config/chromium/NativeMessagingHosts
cat > $HOME/.config/chromium/NativeMessagingHosts/$HOSTNAME.json <<EOF
{
  "name": "$HOSTNAME",
  "description": "mlcchost",
  "path": "$PREFIX/bin/mlcchost",
  "type": "stdio",
  "allowed_origins": [
    "chrome-extension://coldlbhggfimljginjdildejppdidbhc/"
  ]
}
EOF
