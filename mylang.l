%{
#define YYSTYPE int
#include "mylang.tab.hh"

#define YY_USER_ACTION yylloc.first_line = yylloc.last_line = yylineno;
%}
%option yylineno
%option nodefault
%%
:=|<=|>=|==|<>          return yyop(yytext);
[0-9]+                  return yyint(yytext);
[a-zA-Z][a-zA-Z0-9]*    return yyid(yytext);
\"([^"\\]|\\.)*\"       return yystr(yytext);
[ \t\n]+                /* white space */
\/\/.*                  /* comment */
.                       return yyop(yytext);
%%
int yywrap()
{
  return 1;
}
