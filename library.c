#include <stdio.h>
#include <string.h>

void print32(int x) {
  printf("%d", x);
}

void print1(int x) {
  fputs(x ? "yes" : "no", stdout);
}

void printstr(const char *s, int len) {
  fwrite(s, len, 1, stdout);
}

int input() {
  int x;
  while (scanf("%d", &x) != 1)
    puts("Please input an integer.");
  scanf("%*[^\n]");
  return x;
}

static int ask1(int *p)
{
  char s[8];
  scanf("%5s%*[^\n]", s);
  if (!strcasecmp(s, "yes") || !strcasecmp(s, "y")) {
    *p = 1;
    return 1;
  }
  if (!strcasecmp(s, "no") || !strcasecmp(s, "n")) {
    *p = 0;
    return 1;
  }
  return 0;
}

int ask() {
  int p;
  while (!ask1(&p))
    puts("Please input yes or no.");
  return p;
}
